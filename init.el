;;; init.el --- Configuration file                   -*- lexical-binding: t; -*-

;; Copyright (C) 2017

;; Author:  <JosueSCHK>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
										;ToDos
;; TODO: ;imenu
;; TODO: ;Mail
;; TODO: Mejorar escritura (Detectar mayusculas autocapitalize, corregir palabra si es la unica correccion)
;; TODO: Multiple cursors
;; TODO: EXWM
;; TODO: header.h agregar
;; TODO: \n en otro color
;; TODO: Recorda comands
;; TODO: Cambiar color rosa por celeste (invertir)

										;How To
;; Titles: have 100 ; up and down, and the title text must have only one ;
;; Up of the Titles must be a void line ('cause S-p) and down have a ;; whit description
;; MUST BE: Used for elisp code that need to stay on top or end of init
;; Subtitles: have 45 ; up void and the title text must have only one ;
;; Sub-Subtitles: have 41 ; in CamelCase and up a void line
;; If is possible the description of a function must be in the col 65
;; If is possible the description dont have to be truncated
;; When a package is called then all the time is need it again will be closer to the first time
;; TESTING: At least 3 days of use before moving to definitive place
;; If i cant put a commentary then "..."

										;Menu
;; Must Be
;; Basic
;; Packages
;; Programming
;; Buffers
;; Directories
;; Editing-Text
;; Others-Modes
;; Keybindings
;; Random
;; Testing
;; Must-Be

										;Others
;; Reminder: I use Hyper if i dont have it, I can use: C-x @ h

;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;MUST BE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Here, only things that for some reason need to load first
(setq custom-file "~/.emacs.d/custom.el") ;; ------------------- For the builtin customization that no one uses,
(load custom-file 'noerror) ;; --------------------------------- at least have the decency to keep my init clean
(setq gc-cons-threshold 20000000) ;; --------------------------- More memory size
(let ((minver "24.3")) ;; -------------------------------------- In case someone use lower emacs ver.
  (when (version< emacs-version minver)
	(error "This config requires Emacs v%s or higher")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;BASIC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; I cant live without them

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;CustomDisplay
(menu-bar-mode -1) ;; ------------------------------------------ Dont display menu-bar
(tool-bar-mode -1) ;; ------------------------------------------ Dont display tool-bar
(scroll-bar-mode -1) ;; ---------------------------------------- Dont display scroll
(set-default 'cursor-type 'bar) ;; ----------------------------- Cursor is a bar
(setq frame-title-format "Emacs") ;; --------------------------- Static frame name "Emacs"
(setq initial-scratch-message nil) ;; -------------------------- Sin mje en *scratch*
(setq inhibit-startup-message t) ;; ---------------------------- Inhibit startup message
(load "~/.emacs.d/lisp/Shadow-monokai-theme/monokai-theme.el");; Load Theme

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ImportantBasicThings
(fset 'yes-or-no-p 'y-or-n-p) ;; ------------------------------- Use y-n instead of yes-no. Obviously
(set-default 'truncate-lines t) ;; ----------------------------- Truncate lines instead of making the line break
(delete-selection-mode 1) ;; ----------------------------------- Overwrite the selection
(electric-pair-mode 1) ;; -------------------------------------- Autoclose parentheses
(show-paren-mode) ;; ------------------------------------------- Highlight parentheses
(setq column-number-mode t) ;; --------------------------------- Show column number on modeline
(setq inhibit-startup-echo-area-message "shadowkopa") ;; ------- I dont want to have the ugly startup message
(setq current-language-environment "Spanish") ;; --------------- Default Language Spanish
(windmove-default-keybindings) ;; ------------------------------ Move whith Shift-Arrows between the buffers
(setq echo-keystrokes 0.1) ;; ---------------------------------- Show keystrokes in progress

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Basic Keybindings
(global-set-key (kbd "C-c c") 'comment-or-uncomment-region) ;; - Please commente what I selected
(define-key key-translation-map [?\C-h] [?\C-?]) ;; ------------ Translate help
(global-set-key (kbd "<f1>") 'help-command) ;; ----------------- Help command
(global-set-key (kbd "M-h") 'backward-kill-word) ;; ------------ Delete backward word
(global-set-key (kbd "C-x k") 'kill-this-buffer) ;; ------------ Kill actual buffer and dont ask me

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;LessImportant
(setq shift-select-mode nil) ;; -------------------------------- Emacs knights don't use shift to mark things
(global-subword-mode 1) ;; ------------------------------------- Easily navigate sillycased words
(setq delete-by-moving-to-trash t) ;; -------------------------- Move files to trash when deleting
(auto-compression-mode t) ;; ----------------------------------- Transparently open compressed files
(setq tab-width 4) ;; ------------------------------------------ Hard tabs
(setq-default tab-width 4 indent-tabs-mode t) ;; --------------- Tab width 4
(set-default 'sentence-end-double-space nil);; ----------------- Sentences dont need double spaces to end. Period
(setq line-number-mode t) ;; ----------------------------------- If is not there show line number.
(setq locale-coding-system 'utf-8) ;; -------------------------- UTF-8 please
(set-terminal-coding-system 'utf-8) ;; ------------------------- In my terminal too
(set-keyboard-coding-system 'utf-8) ;; ------------------------- More UTF-8
(set-selection-coding-system 'utf-8) ;; ------------------------ Even more
(prefer-coding-system 'utf-8) ;; ------------------------------- Final UTF-8 thing
(global-set-key (kbd "C-s") 'isearch-forward-regexp) ;; -------- Use search with Regular Expressions that
(global-set-key (kbd "C-r") 'isearch-backward-regexp) ;; ------- some people think is better
(global-set-key (kbd "C-M-s") 'isearch-forward) ;; ------------- ...
(global-set-key (kbd "C-M-r") 'isearch-backward) ;; ------------ ...

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;PACKAGES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t); Add melpa repositorie
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages") t);Add Marmalade
(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;REQUIREs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'anzu) ;; --------------------------------------------- How many words are find it
(require 'bm) ;; ----------------------------------------------- Save a posicion on the screen
(require 'cc-mode)
(require 'comment-tags) ;; ------------------------------------- Todos info and bugs
(require 'dash)
(require 'diminish) ;; ----------------------------------------- Diminish modeline clutter
(require 'dired)
(require 'eshell)
(require 'eyebrowse) ;; ---------------------------------------- Multi-Workspaces
(require 'flycheck)
(require 'google-translate) ;; --------------------------------- Useful for no English speakers
(require 'guess-language);; ------------------------------------ Guess what language im working
(require 'hippie-exp)
(require 'ido)
(require 'imenu)
(require 'key-chord) ;; ---------------------------------------- Fast keybinding
(require 'popup-kill-ring)
(require 'rainbow-mode) ;; ------------------------------------- Show me Hexadecimal colors
(require 'real-auto-save)
(require 'saveplace)
(require 'sh-script)
(require 'smex)
(require 'smooth-scrolling) ;; --------------------------------- Keep cursor away from edges
(require 'switch-window)
(require 'undo-tree) ;; ---------------------------------------- Tree of edits
(require 'uniquify) ;; ----------------------------------------- Add directory to the buffer name if not unique
(require 'web-mode)
(require 'yasnippet)
(require 'goto-chg)
(require 'highlight-indent-guides)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;LOAD-PHATS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defconst site-lisp-dir "~/.emacs.d/settings") ;; Path to dependencies;TODO: q?
(defconst settings-dir "~/.emacs.d/site-lisp") ;; Path to dependencies
(add-to-list 'load-path settings-dir) ;; ----------------------- Auto-load
(add-to-list 'load-path site-lisp-dir) ;; ---------------------- Auto-Load ;ERROR

;; All paths: This keeps the directory clean :3
(setq bm-repository-file           "~/.emacs.d/settings/bm-repository")
(setq ac-comphist-file             "~/.emacs.d/settings/auto-complete.dat")
(setq save-place-file              "~/.emacs.d/settings/save-places")
(setq smex-save-file               "~/.emacs.d/settings/smex-items")
(setq ido-save-directory-list-file "~/.emacs.d/settings/ido.last")
(setq auto-save-list-file-prefix   "~/.emacs.d/settings/auto-save-list/save-")
(setq auto-save-list-file-name     "~/.emacs.d/settings/auto-save-list/current")
(setq ido-save-directory-list-file "~/.emacs.d/settings/ido.last")
(setq eshell-directory-name        "~/.emacs.d/settings/eshell/")
(defconst my-backup-dir            "~/.emacs.d/settings/backup")

(defmacro λ (&rest body);; ------------------------------------- Shorthand for interactive lambda
  "BODY. Shorthand for interactive lambdas."
  `(lambda ()
     (interactive)
     ,@body))
(global-set-key (kbd "H-L") (λ (insert "\u03bb"))) ;; --------- Fastest add of λ character

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;PROGRAMMING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Change something about the modes included in prog-mode? Is here
(load "~/.emacs.d/lisp/folder-creator.el") ;; ------------------ Load folder-creator
(load "~/.emacs.d/lisp/auto-insert.el") ;; --------------------- Load auto-insert (Used for author-comm)
(add-hook 'after-init-hook #'global-flycheck-mode) ;; ---------- Correct Sintax
;;(add-hook 'prog-mode-hook 'linum-mode) ;; ---------------------- Show me Line Number
(yas-global-mode 1) ;; ----------------------------------------- Snippets
(ac-config-default) ;; ----------------------------------------- AutoComplete
(setq ac-ignore-case nil) ;; ----------------------------------- Avoid auto-capitalize words
(setq real-auto-save-interval 20) ;; --------------------------- Interval for auto-save in seconds
(setq-default comment-empty-lines nil ;; ----------------------- Basic Indents
	        tab-width 4
	        c-basic-offset 4
	        cperl-indent-level 4
	        indent-tabs-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;RainbowMode
(add-hook 'emacs-lisp-mode-hook 'rainbow-mode) ;; -------------- Hexadecimal colors in emacs-lisp
(add-hook 'web-mode-hook 'rainbow-mode) ;; --------------------- and web (php/html)
(add-hook 'css-mode-hook 'rainbow-mode) ;; --------------------- but more important in Css

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;CommentTags
(autoload 'comment-tags-mode "comment-tags-mode") ;; ----------- ...
(setq comment-tags-keymap-prefix (kbd "C-c t")) ;; ------------- Ok, this doesnt work so FIXME: C-c # b
(autoload 'comment-tags-mode "comment-tags-mode")
(with-eval-after-load "comment-tags"
  (setq comment-tags-keyword-faces
		`(("TODO" . ,(list :weight 'bold :foreground "#28ABE3"))
		  ("FIXME" . ,(list :weight 'bold :foreground "#FF8C1A"))
		  ("BUG" . ,(list :weight 'bold :foreground "#DB3340"))
		  ("INFO" . ,(list :weight 'bold :foreground "#F7EAC8"))))
  (setq comment-tags-comment-start-only nil ;; ----------------- Accept Tags in everyplace of a comment
		comment-tags-require-colon t        ;; ----------------- Yes ':' is need it
		comment-tags-case-sensitive t       ;; ----------------- TODO in not the same that todo
		comment-tags-show-faces t           ;; ----------------- Colors in C-c #-b
		comment-tags-lighter nil))          ;; ----------------- Something about to set modeline text
(add-hook 'prog-mode-hook 'comment-tags-mode) ;; --------------- And in every programing mode

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;HighlightGuides
(setq highlight-indent-guides-method 'character) ;; ------------ Show guides for structs like for or if
(setq highlight-indent-guides-auto-enabled nil)
(set-face-background 'highlight-indent-guides-odd-face "dark turquoise");;Color
(set-face-background 'highlight-indent-guides-even-face "turquoise")
(set-face-foreground 'highlight-indent-guides-character-face "turquoise")
(define-key prog-mode-map (kbd "C-c H-l") 'highlight-indent-guides-mode);;Keybinding
(define-key c-mode-map (kbd "C-c H-l") 'highlight-indent-guides-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;WEB-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode)) ;; -- Web-mode edit my html files
(add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode)) ;; --- And also my phps
(defun my-web-mode-hook () ;; ---------------------------------- ...
  "Hooks for Web mode." ;; ------------------------------------- ...
  (setq web-mode-markup-indent-offset 2) ;; -------------------- ...
  (setq-default indent-tabs-mode nil) ;; ----------------------- ...
  (emmet-mode t)) ;; ------------------------------------------- ...
(add-hook 'web-mode-hook  'my-web-mode-hook) ;; ---------------- And I use emmet for AC
(setq-default web-mode-markup-indent-offset tab-width) ;; ------ Some indent stuff
(setq-default web-mode-css-indent-offset tab-width) ;; --------- ...
(setq-default web-mode-code-indent-offset tab-width) ;; -------- ...
(setq-default web-mode-sql-indent-offset tab-width) ;; --------- ...

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;C-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-key c-mode-base-map (kbd "C-c r") 'compile) ;; --------- Easy Compile
(define-key c-mode-base-map (kbd "H-m") 'man-follow) ;; -------- Manual Entry of actual command
(define-key c-mode-map (kbd "H-M") 'man) ;; -------------------- Manual Entry
(setq compile-command "cd .. && make -k") ;; ------------------- Using Folder-Creator Makefile is not in src
(define-key c-mode-map (kbd "H-;") (λ (move-end-of-line 1) (insert ";"))); Add final ;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;EMACS-LISP-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun eval-buffer-or-region () ;; ----------------------------- If i had something selected I want to eval
  "Eval the region if is active." ;; --------------------------- only that region
  (interactive)
  (if mark-active
      (progn
        (eval-region (region-beginning) (region-end))
        (pop-mark)
        (message "Region evaluated"))
    (progn
      (eval-buffer)
      (message "Buffer evaluated"))))
(define-key emacs-lisp-mode-map (kbd "C-c r") 'eval-buffer-or-region); Eval buffer or region
(define-key lisp-interaction-mode-map (kbd "C-c r") 'eval-buffer-or-region);Eval buffer or region
(define-key lisp-mode-map (kbd "C-c r") 'eval-buffer-or-region);Eval buffer or region

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;SHELL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun eshell-here () ; ---------------------------------------- Open the eshell where I am
  "Opens a new shell in the directory associated with the current buffers file.
The eshell is renamed to match that
directory to make multiple eshell windows easier."
  (interactive)
  (let* ((parent (if (buffer-file-name)
                     (file-name-directory (buffer-file-name))
                   default-directory))
         (height (/ (window-total-height) 3))
         (name   (car (last (split-string parent "/" t)))))
    (split-window-vertically (- height))
    (other-window 1)
    (eshell "new")
    (rename-buffer (concat "*eshell: " name "*"))
    (insert (concat "ls"))
    (eshell-send-input)))
(global-set-key (kbd "H-T") 'eshell-here) ; -------------------- KB similar to H-t

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;SH-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-key sh-mode-map (kbd "H-m") 'man-follow)
(define-key sh-mode-map (kbd "H-M") 'man) ;; ------------------- Look man entry
(define-key sh-mode-map (kbd "C-c C-l") 'run-sh-script) ;; ----- Recompile doesnt respect current project
(define-key sh-mode-map (kbd "H-;") (λ (move-end-of-line 1) (insert ";")));;Faster than a keychord

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;LATEX
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'auto-complete-auctex)
(setq Tex-auto-save t)
(setq Tex-parse-self t)
;;(add-hook 'LaTeX-mode-hook 'latex-preview-pane-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;BUFFERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; If edit something about buffers, window or frames. Is here
(load "~/.emacs.d/lisp/ventanas.el") ;; ------------------------ Load my frames configurations
(global-set-key (kbd "C-c 1") 'program-windows) ;; ------------- Programming Mode
(global-set-key (kbd "C-c 2") 'text-windows) ;; ---------------- Text Mode
(global-set-key (kbd "C-c 3") 'elisp-windows) ;; --------------- Init Mode
(global-set-key (kbd "C-c 4") 'fourplus-windows) ;; ------------ Four divided Mode
(global-set-key (kbd "C-c 0") 'default-windows) ;; ------------- Default Mode
(winner-mode t) ;; --------------------------------------------- Undo and Redo changes on frammes
(global-hl-line-mode);; ---------------------------------------- Highlight actual line
(save-place-mode t) ;; ----------------------------------------- Save point position between sessions
(global-auto-revert-mode 1) ;; --------------------------------- Update buffer if is edited in disc
(setq global-auto-revert-non-file-buffers t) ;; ---------------- Auto refresh dired
(setq auto-revert-verbose nil) ;; ------------------------------ Less verbose on revertions
(defun kill-and-close () ;; ------------------------------------ Kill and close buffer
  "Kill current buffer and close it."
  (interactive)
  (kill-this-buffer)
  (delete-window))
(global-set-key (kbd "H-x k") 'kill-and-close) ;; -------------- KB similar to C-x k

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MultiWorkspaces
(eyebrowse-mode t) ;; ------------------------------------------ A lot of workspaces, When buffers is not enough
(global-set-key (kbd "H-1") 'eyebrowse-switch-to-window-config-1)
(global-set-key (kbd "H-2") 'eyebrowse-switch-to-window-config-2)
(global-set-key (kbd "H-3") 'eyebrowse-switch-to-window-config-3)
(global-set-key (kbd "H-4") 'eyebrowse-switch-to-window-config-4)
(global-set-key (kbd "H-5") 'eyebrowse-switch-to-window-config-5)
(global-set-key (kbd "C-c H-1") 'eyebrowse-create-window-config);I dont need to spend my 10 numbers
(global-set-key (kbd "H-Z") 'eyebrowse-prev-window-config);; --- Move to the left window
(global-set-key (kbd "H-z") 'eyebrowse-next-window-config);; --- Move to the right window
(global-set-key (kbd "C-x H-c") 'eyebrowse-close-window-config); Close actual window
(global-set-key (kbd "C-H-x") 'eyebrowse-rename-window-config);; Use a Tag in the window

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;SmoothScrolling
(smooth-scrolling-mode 1) ;; ----------------------------------- Keep cursor away from edges when scrolling

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Custom bookmarks
(define-key prog-mode-map (kbd "C-c n") 'bm-toggle) ;; --------- In prog-mode
(define-key prog-mode-map (kbd "C-c C-n") 'bm-next) ;; --------- ...
(define-key prog-mode-map (kbd "C-c C-p") 'bm-previous) ;; ----- ...
(define-key c-mode-map (kbd "C-c n") 'bm-toggle) ;; ------------ In c-mode too
(define-key c-mode-map (kbd "C-c C-n") 'bm-next) ;; ------------ ...
(define-key c-mode-map (kbd "C-c C-p") 'bm-previous) ;; -------- ...

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;DIRECTORIES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;If edit the method that you view the directory is here
(global-set-key [f8] 'neotree-toggle) ;; ----------------------- Neotre
(setq uniquify-buffer-name-style 'forward) ;; ------------------ Add parts of directory to the buffer name if not unique
;; (defconst my-backup-dir ;; ------------------------------------- Backups saved in a file
;;   (expand-file-name (concat user-emacs-directory "backups"))) ;; ...
(setq make-backup-files t ;; ----------------------------------- Make backup first time a file is saved
	  version-control t   ;; ----------------------------------- Number and keep versions of backups
	  backup-by-copying t ;; ----------------------------------- Copy (don't clobber symlinks) them to...
	  backup-directory-alist `(("."	.	,my-backup-dir)) ;; ---- ...here
	  kept-new-versions 2
	  kept-old-versions 5
	  delete-old-versions t ;; -------------------------------- Don't ask about deleting old versions
	  auto-save-default nil) ;; ------------------------------- No auto saves to #file#
(if (not (file-exists-p my-backup-dir));;---------------------- And if the folder doesnt exists, create it
	(mkdir my-backup-dir t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Ido
(ido-mode t) ;; ------------------------------------------------ I want to see all the files
(ido-everywhere t)
(setq ido-case-fold t) ;; -------------------------------------- Case sensitive on folders
(setq ido-auto-merge-work-directories-length -1) ;; ------------ But I dont want to search in others folders Duh

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;LinuxFilesIntegration
(add-to-list 'auto-mode-alist '("\\.service\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.timer\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.target\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.mount\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.automount\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.slice\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.socket\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.path\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.netdev\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.network\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.link\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.automount\\'" . conf-unix-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;EDITING-TEXT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; If is about the text, how is present it, how to edit it or how work. (Not in a mode) Is here
(define-skeleton quoted-questions ;; --------------------------- If I open a ¿ please close it
	  "Insert ¿...?"
	  nil "¿"_"? ") ;; BUG: Inserta un salto de linea
(global-set-key "¿" 'quoted-questions) ;; ---------------------- And binding it in the right place
(define-skeleton quoted-exclamation ;; ------------------------- If I open a ¡ please close it
	  "Insert ¡...!"
	  nil "¡"_"! " 'skeleton-end-hook)
(global-set-key "¡" 'quoted-exclamation) ;; -------------------- And binding it in the right place
;;(add-hook 'before-save-hook 'delete-trailing-whitespace) ;; ---- Clean whitespaces for me. Please

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;DeleteLine
(defun delete-current-line () ;; ------------------------------- Delete all the line and not save it in killRing
  "Delete (not kill) the current line."
  (interactive)
  (save-excursion
	(delete-region
	 (progn (forward-visible-line 0) (point))
	 (progn (forward-visible-line 1) (point)))))
(global-set-key (kbd "C-k") 'delete-current-line) ;; ----------- Binding in the old kill-line

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;CopyOrCutLine
(defadvice kill-ring-save (before slick-copy activate compile);; Copy line even if is not selected
  "Kill and save the actual line."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
	 (message "Copied line")
	 (list (line-beginning-position)
		   (line-beginning-position 2)))))
(defadvice kill-region (before slick-cut activate compile) ;; -- Cut line even if is not selected
  "Kill the actual line."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
	 (list (line-beginning-position)
		   (line-beginning-position 2)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GoogleTranslate
(setq google-translate-default-source-language "en") ;; -------- Emacs language
(setq google-translate-default-target-language "es") ;; -------- Translate to Spanish

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;UndoTree
(global-undo-tree-mode) ;; ------------------------------------- Show me how I edit the file in everyplace

(defun untabify-buffer () ;; ----------------------------------- Quits all tabs editing-text
  "Quits all tabs."
  (interactive)
  (untabify (point-min) (point-max)))

(defun indent-buffer () ;; ------------------------------------- Indent buffer without going to the beggining
  "Tabs all buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

(defun comment-kill-all () ;; ---------------------------------- Kill all comments in the actual buffer
  "Delete all comments on buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (comment-kill (save-excursion
                    (goto-char (point-max))
                    (line-number-at-pos)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;SEARCHS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq case-fold-search t) ;; ----------------------------------- Insensitive to upp/lowercase in searches
(global-anzu-mode +1)  ;; -------------------------------------- How many words are find it
(set-face-attribute 'anzu-mode-line nil
					:foreground "#A6E22E") ;; ------------------ With a color that dont hurt me

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;OTHERS-MODES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Every mode that doesnt seem to have a place before

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;TEXT-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'text-mode-hook 'visual-line-mode) ;; ---------------- Do not truncate lines, follow below
;;(add-hook 'text-mode-hook 'flyspell-mode) ;; ----------------- Maybe is a little distracting
(setq ispell-dictionary "castellano") ;; ----------------------- Lenguage default
(setq guess-language-languages '(es en)) ;; -------------------- Guess lenguage between Spanish and English
(setq guess-language-min-paragraph-length 35) ;; --------------- How many word its need it to select a language
(add-hook 'text-mode-hook (lambda () (guess-language-mode 1))) ; Start when I enter text-mode
(setq flyspell-duplicate-distance 0) ;; ------------------------ Disable horrible and confusing Flyspell “duplicate” marks
(define-key text-mode-map (kbd "C-c $") 'ispell) ;; ---------------------- Correct syntax


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;VIEW-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq view-read-only t) ;; ------------------------------------- When I open a Read-Only File
(global-set-key (kbd "H-r") 'view-mode) ;; --------------------- Easy to catch when it is need it
(defvar view-mode-map) ;; -------------------------------------- Is not a free variable, shut up emacs
(add-hook 'view-mode-hook 'my-view)
(defun my-view () ;; ------------------------------------------- Yes, I'm lazy
  "In Read-Only I don't have to worry about C to move."
(define-key view-mode-map "n" 'next-line)
(define-key view-mode-map "p" 'previous-line)
(define-key view-mode-map "f" 'forward-char)
(define-key view-mode-map "b" 'backward-char)
(define-key view-mode-map "a" 'move-beginning-of-line)
(define-key view-mode-map "e" 'move-end-of-line)
(define-key view-mode-map "F" 'forward-word)
(define-key view-mode-map "B" 'backward-word)
(define-key view-mode-map "l" 'recenter-top-bottom)
(define-key view-mode-map "P" 'backward-paragraph)
(define-key view-mode-map "N" 'forward-paragraph)
(define-key view-mode-map "<" 'beginning-of-buffer)
(define-key view-mode-map ">" 'end-of-buffer))
;; (add-hook 'emacs-lisp-mode-hook 'view-mode) ;; ----------------- Usually I open .el to read it
;; (add-hook 'lisp-interaction-mode-hook 'view-mode-disable) ;; --- But Ex. *Scratch* in not a .el so disable it
;; (add-hook 'help-mode-hook 'view-mode) ;; ----------------------- Help-Mode too

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;*SCRATCH*
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun create-scratch-buffer nil ;; ---------------------------- Create a new and different scratch
  "Create a new scratch buffer to work in. (could be *scratch* - *scratchX*)."
  (interactive)
  (let ((n 0) bufname)
	(while (progn
			 (setq bufname (concat "*scratch"
								   (if (= n 0) "" (int-to-string n))
								   "*"))
			 (setq n (1+ n))
			 (get-buffer bufname)))
	(switch-to-buffer (get-buffer-create bufname))
	(lisp-mode)))
(global-set-key (kbd "C-c b") 'create-scratch-buffer) ;; ------- KB similar to switch buffers

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;KEYBINDINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;General keybindings and keychords

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Globals
(global-set-key (kbd "M-x") 'smex) ;; -------------------------- Mode-line whit prediction
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command);; Original M-x
(global-set-key (kbd "C-S-Y") 'popup-kill-ring) ;; ------------- Show clipboard
(global-set-key (kbd "C-M-k") 'kill-whole-line)   ;; ----------- Delete actual line and save it in kill-ring
(global-set-key (kbd "C-c SPC") 'whitespace-cleanup) ;; -------- Clean Spaces
(define-key global-map (kbd "C-c ;") 'iedit-mode) ;; ----------- Variables remplace
(global-set-key "\C-c\C-m" 'execute-extended-command) ;; ------- Effective Emacs
(global-set-key (kbd"H-t") 'shell-pop) ;; ---------------------- A shell appears at top of the buffers
(global-set-key (kbd "C-c H-c") 'count-words) ;; --------------- Count total characters and lines
(global-set-key (kbd "<menu>") 'linum-mode) ;; ----------------- Optimized use of linum mode
(global-set-key [(control meta .)] 'goto-last-change) ;; ------- Go to the last change

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;NIL
(global-set-key (kbd "C-z") nil) ;; ---------------------------- A veces minimizaba sin querer
(global-set-key (kbd "ESC ESC ESC") nil) ;; -------------------- Se cerraba sin querer

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Movements
(global-set-key (kbd "<C-S-up>")     'buf-move-up) ;; ---------- Switch Buffers
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)
(global-set-key (kbd "C-M-z") 'switch-window) ;; --------------- Move between Buffers
(global-set-key (kbd "M-z") 'windmove-right)
(global-set-key (kbd "C-z") 'windmove-down)
(global-set-key (kbd "C-S-Z") 'windmove-up)
(global-set-key (kbd "M-Z") 'windmove-left)
(global-set-key "\M-p" 'backward-paragraph) ;; ----------------- Fastes movement
(global-set-key "\M-n" 'forward-paragraph)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;KeyChords
(key-chord-mode 1) ;; ------------------------------------------ Two quick keys
(key-chord-define c-mode-base-map ";;"  "\C-e;");; ------------- Finish sentence with ;
(key-chord-define c-mode-base-map "<<" "<>\C-b");; ------------- Auto-complete <>
(key-chord-define c-mode-base-map "{{" "\C-e{}\C-b\C-m");; ----- Auto-Complete If {}
(key-chord-define web-mode-map ";;" "\C-e;");; ----------------- Finish sentence with ;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;RANDOM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Things that are not important and a little stupids

(setq network-security-level 'paranoid) ;; --------------------- Security is important!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PartyMode
;;(load "~/.emacs.d/lisp/party-mode.el")
(defvar party-state 1 "Initial setting for the `a` global variable.")
(defun party-toggle ()
"Doc-string for `my-switch` function."
(interactive)
  (cond
	((= party-state 1)
	  (party-mode)
	  (setq party-state 2))
	((= party-state 2)
	  (stop-partying)
	  (setq party-state 1)) ) )
(setq party-mode-start-music-fn
  (lambda () (start-process "party-music" "*party-music*" "mpg123" "/home/shadowkopa/.emacs.d/full-party.mp3")))
(setq party-mode-stop-music-fn
  (lambda () (kill-process "party-music")))
(global-set-key (kbd "H-p") 'party-toggle) ;; ------------------ Start and Finish Party

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;COMMENTED-LINES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (global-set-key return (lambda() ;; ----------------------- Function for learning keys
;; 				(interactive)
;; 				(message "Use C-m -.-")))
;; (set-frame-parameter (selected-frame) 'alpha '(99 . 99)) ;; - Transparecy of Emacs
;; (add-to-list 'default-frame-alist '(alpha . (97 . 99)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;TESTING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Testing code go here. INFO: Pasar

;;View-Mode Color 14/11
(defun change-mode-line-color ()
  (interactive)
  (when (get-buffer-window (current-buffer))
    (cond (window-system
           (cond (view-mode
                  (set-face-background 'mode-line "#383838")
                  (set-face-foreground 'mode-line "orange")
                  )
                 (t
                  (set-face-background 'mode-line "#383838")
                  (set-face-foreground 'mode-line "#E3F2C1")))
           )
          (t
           (set-face-background 'modeline
                                (if view-mode "red"
                                  "white"))))))
(add-hook 'view-mode-hook 'change-mode-line-color)

;;Better-defaiults
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-x C-b") 'ibuffer)

(require 'saveplace)
(setq-default save-place t)

;;NICO

;;Quits all tabs editing-text

;; Delete all blank lines
(defun delete-all-blank-lines ()
  "Collapse multiple blank lines from buffer or region into a single blank line."
  (interactive)
  (save-excursion
    (let (min max)
      (if (equal (region-active-p) nil)
          (mark-whole-buffer))
      (setq min (region-beginning) max (region-end))
      (replace-regexp "^\n\\{2,\\}" "\n" nil min max))))

(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer.
Including indent-buffer, which should not be called automatically on save."
  (interactive)
  (untabify-buffer)
  (delete-trailing-whitespace)
  (delete-all-blank-lines)
  (indent-buffer))

(defun cleanup-buffer-hard ()
  "Perform a bunch of operations on the whitespace content of a buffer.
Including indent-buffer, which should not be called automatically on save."
  (interactive)
  (comment-kill-all)
  (untabify-buffer)
  (delete-trailing-whitespace)
  (delete-all-blank-lines)
  (indent-buffer))

(defun run-sh-script ()
  "Open a eshell buffer and execute current script."
  (interactive)
  (defvar run-sh-script-file-name (buffer-file-name))
  (defvar run-sh-script-params "")
  (setq run-sh-script-params (read-string "Enter parameters: "))
  (if (= (count-windows) 1)
      (progn
        (split-window-horizontally)
        (other-window 1))
    (progn
      (other-window 1)))
  (eshell)
  (eshell-return-to-prompt)
  (insert (concat run-sh-script-file-name " " run-sh-script-params))
  (eshell-send-input))

(defun rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(defun delete-current-buffer-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (ido-kill-buffer)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

(defun copy-current-file-path ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))

;;INIT
(set-default 'imenu-auto-rescan t);; Always rescan buffer for imenu
;; Smex configuration
(smex-initialize)
;; Minor customizations

;;Slime
(require 'slime-autoloads)
(load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq inferior-lisp-program "/usr/bin/sbcl")
(slime-setup '(slime-fancy))
(setq lisp-indent-function 'common-lisp-indent-function)
(setq  slime-complete-symbol-function 'slime-fuzzy-complete-symbol)
(define-key slime-mode-map [f12] 'slime-arglist)
;; (slime-startup-animation nil)
;;(add-hook 'after-init-hook 'change-mode-line-color)

;;;;SQL
(require 'sql)
(sql-set-product-feature 'mysql :prompt-regexp
						 "^\\(MariaDB\\|MySQL\\) \\[[_a-zA-Z]*\\]> ")

(setq sql-mysql-login-params
      '((user :default "root")
        ;; (database :default "postgres")
        (server :default "localhost")
        (port :default 5432)
		(password)
		))
;;; hooks
(add-hook 'sql-interactive-mode-hook
          (lambda ()
            (toggle-truncate-lines t)
            (setq-local show-trailing-whitespace nil)
            (auto-complete-mode t)))

(add-hook 'sql-mode-hook
          (lambda ()
            (setq-local ac-ignore-case t)
(auto-complete-mode)))

;;Python
(elpy-enable)
(define-key python-mode-map (kbd "C-c r") 'elpy-shell-send-region-or-buffer)

;;Yass
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
(define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
										;MUST-BE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Things that must be at the end of the init file

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Diminish
;(diminish 'wrap-region-mode)
(diminish 'undo-tree-mode "Ut")
(diminish 'anzu-mode)
(diminish 'rainbow-mode)
(diminish 'subword-mode)
(diminish 'company-mode "Cmy")
(diminish 'guess-language-mode)

(setq gc-cons-threshold 4000000) ;; ---------------------------- Should't be too big
(message "init.el read to end") ;; ----------------------------- Told me that everithing is alright
(provide 'init)

;;; init.el ends here
