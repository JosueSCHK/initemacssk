;;; mode-line
;;; Commentary:


;;; Code:
(setq-default mode-line-format
	      (list
	       " "
       ;; BUFFER NAME; the file name as a tool tip
       '(:eval (propertize "%b" 'face 'mode-line-filename-face
			   'help-echo (buffer-file-name)))

	       ;; Lectura/Escritura
       '(:eval
	(cond (buffer-read-only
	       (propertize "  RO " 'face 'mode-line-read-only-face))
	      ((buffer-modified-p)
	       (propertize "    " 'face 'mode-line-modified-face))
	      (t
	       (propertize "    " 'face 'mode-line-saved-face))))

       ;;
       ;;

       ;; MAJOR MODE
   
       '(:eval (propertize "%m" 'face 'mode-line-mode-face
			   'help-echo buffer-file-coding-system))
       " --- "

       ;; LINEA / COLUMNA

        ;; '%02' to set to 2 chars at least; prevents flickering
       (propertize "[%l-" 'face 'mode-line-position-face)
       (propertize "%c]" 'face 'mode-line-position-face)
       
       ;;POSICION
       ;; relative position, size of file
       " ["
       (propertize "%p" 'face 'font-lock-constant-face) ;; % above top
       "]"
      ; (propertize "%M" 'face 'font-lock-constant-face)
       ;; i don't want to see minor-modes; but if you want, uncomment this:
       ;; minor-mode-alist  ;; list of minor modes


       ;; COLORES
       (make-face 'mode-line-modified-face);Modificado
       (make-face 'mode-line-read-only-face);Solo lectura
       (make-face 'mode-line-saved-face); Guardado

       (make-face 'mode-line-filename-face);Nombre Archivo
       (make-face 'mode-line-mode-face);Maor Mode
       (make-face 'mode-line-position-face);Posicion



       ;;Editar CAJA
       (set-face-attribute 'mode-line nil
			   :foreground "gray86" :background "gray15";foreground letras;background fondo
			   :inverse-video nil
			   :box '(:line-width 1 :color "gray20" :style nil))
       (set-face-attribute 'mode-line-inactive nil
			   :foreground "gray60" :background "gray3"
			   :inverse-video nil
			   :box '(:line-width 1 :color "gray7" :style nil))
       ;;

       ;;ARCHIVO CON CAMBIOS
       (set-face-attribute 'mode-line-modified-face nil
			   :inherit 'mode-line-face
			   :foreground "red"
			   ;:background "#262626"
			   )
       ;;ARCHIVO GUARDADO
       (set-face-attribute 'mode-line-saved-face nil
			   :inherit 'mode-line-face
			   :foreground "#2ebd59"
			   )
       ;;
       ;;SOLO LECTURA
       (set-face-attribute 'mode-line-read-only-face nil
			   :inherit 'mode-line-face
			   :foreground "red"
			   :box '(:line-width 1 :color "gray9"))
       ;;
       ;;Nombre Archivo
       (set-face-attribute 'mode-line-filename-face nil
			   :inherit 'mode-line-face
			   :foreground "#eab700"
			   :weight 'bold)
       ;;
       ;;Major Mode
       (set-face-attribute 'mode-line-mode-face nil
			   :inherit 'mode-line-face
			   :foreground "gray80")
       ;;
       ;;Linea / Columna
       (set-face-attribute 'mode-line-position-face nil
			   :inherit 'mode-line-face
			   :foreground "#aaffdf"
			   )
       

       ))

;;; mode-line.el ends here
