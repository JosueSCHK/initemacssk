(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
	("6f16700a947c63bf31012ad630bd0876300260c6b324a6fed3e11b62686ba68d" default)))
 '(eyebrowse-mode-line-separator "-")
 '(eyebrowse-mode-line-style t)
 '(eyebrowse-new-workspace t)
 '(jdee-jdk (quote ("1.8")))
 '(jdee-jdk-registry
   (quote
	(("1.8" . "/usr/lib/jvm/java-8-jdk")
	 ("1.8" . "/usr/lib/jvm/java-8-openjdk"))))
 '(jdee-server-dir "~/.emacs.d/myJars")
 '(package-selected-packages
   (quote
	(elpy highlight-indent-guides goto-chg sudo-edit auctex auto-complete-auctex latex-preview-pane slime keymap-utils real-auto-save highlight-symbol minesweeper magit chess smartparens diminish wrap-region undo-tree async fuzzy ac-html popup-kill-ring try zenburn google-translate xkcd guess-language rotate shell-pop neotree buffer-move key-chord lorem-ipsum flycheck flycheck-clang-analyzer web-mode switch-window smex anzu ac-emmet ac-clang ac-c-headers)))
 '(shell-pop-full-span t)
 '(shell-pop-shell-type
   (quote
	("ansi-term" "*ansi-term*"
	 (lambda nil
	   (ansi-term shell-pop-term-shell)))))
 '(shell-pop-term-shell "/bin/zsh")
 '(shell-pop-universal-key "H-t")
 '(shell-pop-window-position "top")
 '(shell-pop-window-size 30))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(message "Custom read to end")
