;;; Compiled snippets and support files for `emacs-lisp-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'emacs-lisp-mode
					 '(("akey" "'(:key \"$1\" :description \"$0.\")\n" "cheatsheet entry" nil nil nil "/home/shadowkopa/.emacs.d/snippets/emacs-lisp-mode/cheatsheetcheatsheet entry" nil nil)
					   ("acs" "(cheatsheet-add :group '$1\n                :key \"$2\"\n                :description \"$0.\")\n" "cheatsheet" nil nil nil "/home/shadowkopa/.emacs.d/snippets/emacs-lisp-mode/cheatsheet" nil nil)))


;;; Do not edit! File generated at Wed May 30 16:17:56 2018
