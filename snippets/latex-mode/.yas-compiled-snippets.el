;;; Compiled snippets and support files for `latex-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'latex-mode
					 '(("figr" "\\begin{figure}[h]\n  \\centering\n  \\includegraphics[width=${1:size}]{${2:name}}\n\\end{figure}$0\n" "figr" nil nil nil "/home/shadowkopa/.emacs.d/snippets/latex-mode/figr" nil nil)))


;;; Do not edit! File generated at Wed May 30 16:17:56 2018
