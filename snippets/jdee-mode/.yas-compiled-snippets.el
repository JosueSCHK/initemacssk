;;; Compiled snippets and support files for `jdee-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'jdee-mode
					 '(("for" "for ( ${1:int} ${2:i} = ${3:0}; $2 <= ${4:MAX}; $2++) {\n	$0\n}\n" "for" nil nil nil "/home/shadowkopa/.emacs.d/snippets/jdee-mode/for" nil nil)))


;;; Do not edit! File generated at Wed May 30 16:17:56 2018
