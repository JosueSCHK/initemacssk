;;; package --- Summary
;;; Commentary:

;=============================
;;;;;;;;;;;;;;;VENTANAS;;;;;;;
;=============================

;;; Code:

;;Maximizado
;(toggle-frame-maximized)
;
;;Ventanas moviendose con S-flechas
(windmove-default-keybindings)
;
(defun kill-all-buffers () "Esto es documentacion."
  (interactive)
  "Crear distitas ventanas."
  (mapcar 'kill-buffer (buffer-list))
  (delete-other-windows))
;
;;Cargar modos
(load "~/.emacs.d/lisp/ventanas/program.el")
(load "~/.emacs.d/lisp/ventanas/text.el")
(load "~/.emacs.d/lisp/ventanas/default.el")
(load "~/.emacs.d/lisp/ventanas/elisp.el")
(load "~/.emacs.d/lisp/ventanas/FourPlus.el")

;;; ventanas.el ends here
