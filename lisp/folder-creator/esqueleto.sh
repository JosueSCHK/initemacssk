#!/bin/bash

#$1 = MODO (c-mode, html-mode, etc)
#$2 = Ubicacion del archivo /home/nico/Sandbox......
#$3 = Nombre del archivo en lisp (ejemplo).c

#Primero verifico si me paso los tres parametros
if [ $# == 3 ]; then
    #La cantidad de parametros estan bien!
    #Ya que sabemos que la cantidad de parametros estan bien
    #Podemos definir variables del programa, posiblemente cada uno tendrá que ajustarlas
    dirbase=~/.emacs.d/lisp/folder-creator;
    #    mkdir -p "$2";
    if [ $1 = "c-mode" ]; then
        cp -R $dirbase/c-project "$2"; #Create the folder
        mv $2c-project $2/$3; #Change the folder project name to the project name
		dirbase=$2$3;     #Esta variable contiene la ubicacion del esqueleto en /src
        ######### Configuracion del make ##########
        sed -i "1i TARGET=$3" $dirbase/Makefile; # agrego una linea al principio del makefile que
		
        #contenga el nombre de la aplicacion
    elif [ $1 = "latex-mode" ]; then
		cp -R $dirbase/latex-project "$2";
		mv $2latex-project/index.tex "$2latex-project/$3.tex";
		mv $2latex-project $2/$3;
		dirbase=$2/$3;
		echo -n $dirbase/$3.tex;
    else
        echo "Major mode: $1";
    fi
else
    echo "Parametros incorrectos! Major mode: $1";
    exit 0;
fi
