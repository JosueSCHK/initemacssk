;;; create-carpet-pr.el --- provide parammeters to script

;; Name: Emacs-Shell Programming Carpets
;; Version: 0.2

;;; Commentary:

;; This package simply sends the parameters of the current buffer to a script and open the new file

;;; Code:

(defun folder-creator ()
  "Crea esqueleto para un proyecto base."
  (interactive)
  (setq new-dir-files
        (shell-command-to-string (format "%s %s %s %s" "~/.emacs.d/lisp/folder-creator/esqueleto.sh"
                                         major-mode default-directory  (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))))
  (kill-this-buffer)
  (find-file new-dir-files))

;;; create-carpet-pr.el ends here

