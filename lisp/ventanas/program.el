;;; package --- Summary
;;;Code:
;;Ventana de trabajo para programacion
;;; Commentary:
;;
(defun program-windows ()
  (interactive)
  ;;(kill-all-buffers)
  (delete-other-windows)
  (switch-to-buffer "*scratch*")
  (c-mode)
  ;;A
  (split-window-horizontally)
  ;;A|B
  (split-window-vertically)
  ;;A|B
  ;;C|
  (enlarge-window 1) ;;Mas Abajo
  (enlarge-window 1)
  (next-multiframe-window) ;;Estoy en C
  (split-window-horizontally);;Creo G
  ;;A  |B
  ;;C|G|
  (next-multiframe-window) ;;Estoy en G
  (next-multiframe-window) ;;Estoy en B

  (split-window-horizontally);;Creo E
  ;;A  |B|E
  ;;C|G| |
  (split-window-vertically);;Creo D
  ;;A  |B|E
  ;;C|G|D|
  (enlarge-window 2) ;;Mas Abajo

  (next-multiframe-window);;Estoy en D
  (switch-to-buffer "*compilation*")
  (next-multiframe-window);;Estoy en E
  (split-window-vertically);;Creo F
  ;;A  |B|E
  ;;C|G|D|F
  (enlarge-window 2)
  (next-multiframe-window);;En F
  ;; (term "/bin/zsh");;Abre la terminal
  (next-multiframe-window);;Estoy en A
  )
