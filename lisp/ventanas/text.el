;;; package --- Summary
;;;Code:
;;Ventana de trabajo en texto
;;; Commentary:
;;
(defun text-windows ()
  (interactive)
  ;; (kill-all-buffers)
  (delete-other-windows)
  (switch-to-buffer "*scratch*")
  ;;A
  (split-window-horizontally);;B
  ;;A|B
  (split-window-horizontally);;C
  ;;A|C|B  |
  (next-multiframe-window) ;;Estoy en C
  (next-multiframe-window) ;;Estoy en B
  (split-window-horizontally);;D
  ;;A|C|B|D
  (delete-window);;Elimino B
  ;;A|C |D
  (split-window-vertically);E
  ;;A|C |D
  ;; |E |
  (enlarge-window 23) ;;Mas Abajo
  (next-multiframe-window) ;;Estoy en E
  (next-multiframe-window);;Estoy en D
  (next-multiframe-window);;Estoy en A
  (next-multiframe-window);;Estoy en C
  (text-mode)
  )
