;;; package --- Summary
;;;Code:
;;Ventana de trabajo en Elisp-Emacs
;;; Commentary:
;;
(defun elisp-windows ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  ;;A|B
  (find-file "~/.emacs.d/init.el")
  (next-multiframe-window) ;;Estoy en B
  (switch-to-buffer "*scratch*")
  (next-multiframe-window);;Estoy en A
  (enlarge-window-horizontally 8))
