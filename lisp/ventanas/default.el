;;; package --- Summary
;;;Code:
;;Ventana de trabajo default
;;; Commentary:
;;
(defun default-windows ()
  (interactive)
  (delete-other-windows)
  ;;A
  (switch-to-buffer "*scratch*")
  (split-window-horizontally)
  ;;A|B
  )
(provide 'default)
;;; default.el ends here
