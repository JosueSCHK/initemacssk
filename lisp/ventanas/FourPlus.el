;;; package --- Summary
;;; Code:
;;Ventana de trabajo de cuatro + 1 instancia
;;; Commentary:
;;
(defun fourplus-windows ()
  (interactive)
  (delete-other-windows)
  (switch-to-buffer "*scratch*")
  ;;A
  (split-window-horizontally)
  ;;A|B
  (next-multiframe-window) ;;Estoy en B
  (enlarge-window-horizontally 10) ;;Mas Abajo

  (split-window-horizontally)
  ;;A|B|C
  ;;
  (split-window-vertically)
  ;;A|B|C
  ;; |D
  (next-multiframe-window) ;;Estoy en D
  (next-multiframe-window) ;;Estoy en C
  (split-window-vertically)
  ;;A|B|C
  ;; |D|E
  (next-multiframe-window) ;;Estoy en E
  (next-multiframe-window) ;;Estoy en A

  )
(provide 'FourPlus)
;;; FourPlus.el ends here
