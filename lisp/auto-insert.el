;;; auto-insert.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2017

;; Author:  <nico@Nicolas-Notebook>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(eval-after-load 'autoinsert
  '(define-auto-insert '(c-mode . "C skeleton")
     '(
       "Short description: "
       "/*\n"
       " * Text auto generated with Emacs " emacs-version
       "\n * "(file-name-nondirectory (buffer-file-name))
       " -- " str
       "\n * Written on " (format-time-string "%A, %e of %B, %Y.")
       "\n * Written by Josue S. Chilczuk"
       "\n * E-Mail: JosueSCHk@gmail.com"
       "\n */"
       "\n \n"
       )))

(eval-after-load 'autoinsert
  '(define-auto-insert '(sh-mode . "Sh skeleton")
     '(
       "Short description: "
       "#############################################################\n"
       "# Text auto generated with Emacs " emacs-version
       "\n# "(file-name-nondirectory (buffer-file-name))
       " -- " str
       "\n# Written on " (format-time-string "%A, %e of %B, %Y.")
       "\n# Written by Josue S. Chilczuk"
       "\n# E-Mail: JosueSCHk@gmail.com"
       "\n#############################################################"
       "\n"
       )))

(eval-after-load 'autoinsert
  '(define-auto-insert '(latex-mode . "Latex skeleton")
     '(
       "Short description: "
       "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"
       "% Text auto generated with Emacs " emacs-version
       "\n% "(file-name-nondirectory (buffer-file-name))
       " -- " str
       "\n% Written on " (format-time-string "%A, %e of %B, %Y.")
       "\n% Written by Josue S. Chilczuk"
       "\n% E-Mail: JosueSCHk@gmail.com"
       "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
       "\n"
       )))

(provide 'auto-insert)
;;; auto-insert.el ends here
